import common

MSG = "Burning 'em, if you ain't quick and nimble\n\
I go crazy when I hear a cymbal"

KEY = "ICE"

if __name__ == "__main__":
    print(common.repeatingKeyXor(MSG, KEY))
