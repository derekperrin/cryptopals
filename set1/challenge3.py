import common

# Letter frequencies from:
# https://www.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
cipherHex = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"


if __name__ == "__main__":
    threshhold = 40 # our threshhold for whether a guess is valid
    guesses = common.breakSingleCharXor(cipherHex, threshhold)
    for i in range(len(guesses)):
        print("Guess #" + str(i + 1))
        print(str(guesses[i][0]))
        print("Key: " + str(guesses[i][1]))
        print("Score: " + str(guesses[i][2]))
        print("")
