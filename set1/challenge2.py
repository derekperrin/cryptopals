import common

s1 = "1c0111001f010100061a024b53535009181c"
s2 = "686974207468652062756c6c277320657965"
s3 = "746865206b696420646f6e277420706c6179"

if __name__ == "__main__":
    result = common.xor(s1, s2).decode("ascii")
    print(result)
    if(result != s3):
        print("Actual value of xor'd string does not match expected value.")
