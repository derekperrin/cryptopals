import binascii
import base64
import string   # Needed for a string of all ascii characters
import operator # Needed for itemgetter. See pydocs for usage.

DEF_THRESHHOLD=1000000  # threshhold for scoring with chi-squared

def hex2bin(hexStr):
    return binascii.unhexlify(hexStr)

def bin2b64(rawBytes):
    return base64.b64encode(rawBytes)

def bin2hex(rawBytes):
    return binascii.hexlify(rawBytes)

def xor(s1, s2):
    """
    Takes two hex encoded strings and xor's them together. The strings must be
    the same length.
    """
    if (len(s1) != len(s2)):
        raise ValueError("Input strings are of a different length.")
    # convert both strings from a hex string to raw bytes
    b1 = hex2bin(s1)
    b2 = hex2bin(s2)

    # xor each element of each string, then convert the result into raw bytes
    b3 = bytes(x^y for x,y in zip(b1,b2))

    # return the hex encoded string
    return bin2hex(b3)

def singleCharXor(s, c):
    """
    Takes a hex encoded string ``s`` and xor's it with a character. Returns the
    hex encoded string of the result.
    """
    # copy character 'c' len(s)/2 times, then hex encode it and hexlify.
    # Need to divide by two because 1 byte = 2 hex digits.
    h = "".join(c for i in range(int(len(s)/2)))
    return xor(s,bin2hex(h.encode()))


def score(s, threshhold=DEF_THRESHHOLD):
    """
    Takes a string ``s`` and performs a chi-squared test against the English
    alphabet with known frequencies. A lower score corresponds to a higher 
    probability that the correct key was used for decryption. ``threshold`` is
    an optional parameter that can be passed so the scoring method stops if the
    chi-squared score ever passes the threshhold. Defaults to 1000000.
    """
    # Frequencies of letters in English text.
    # https://www.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
    freqs = { 'a': 0.0812, 'b': 0.0149, 'c': 0.0271, 'd': 0.0432,
             'e': 0.1202, 'f': 0.0230, 'g': 0.0203, 'h': 0.0592,
             'i': 0.0731, 'j': 0.0010, 'k': 0.0069, 'l': 0.0398,
             'm': 0.0261, 'n': 0.0695, 'o': 0.0768, 'p': 0.0182,
             'q': 0.0011, 'r': 0.0602, 's': 0.0628, 't': 0.0910,
             'u': 0.0288, 'v': 0.0111, 'w': 0.0209,'x': 0.0017,
             'y': 0.0211,'z': 0.0007
             }

    chisquare = 0
    for letter in freqs:
        # add ((C_i - E_i)^2/E_i where C_i = count of letter, E_i = expected
        ei = len(s) * freqs[letter]
        ci = s.count(letter) + s.count(letter.upper())
        chisquare += ((ci - ei) ** 2) / ei
        # exit the loop if chisquare is too high.
        if (chisquare > threshhold):
            break
    return chisquare

def breakSingleCharXor(cipherText, threshhold=DEF_THRESHHOLD):
    """
    Takes hex encoded input ``cipherText`` and performs an xor operation on it
    using every letter in the alphabet. It returns the top five results if their
    scores are greater than ``threshhold``.
    """
    # Make the assumption that a key must be a printable character from ascii
    # characters. 
    keyspace = string.printable

    # XOR the cipherText with every key in the keyspace. Score each entry and
    # sort the entries based on their scores. Only keep scores below threshhold.
    # cipherText = hex2bin(cipherText)
    xorHexStrings = [singleCharXor(cipherText, k) for k in keyspace]
    textList = []
    for i in range(len(xorHexStrings)):
        possibleText = str(hex2bin(xorHexStrings[i]))
        keyScore = score(possibleText, threshhold)
        if (keyScore > threshhold):
            continue
        else:
            textList.append((possibleText, keyspace[i], keyScore))

    # Return a list sorted by the second element of each tuple in the list.
    return sorted(textList, key=operator.itemgetter(2))

def detectSingleCharXor(dataFile, threshhold=DEF_THRESHHOLD):
    """
    Takes in an input file, ``dataFile``, and returns a list of the five most 
    likely strings of text that may have been xor'd against a single character.
    Takes a threshhold just like previous functions.
    """
    with open(dataFile) as f:
        lines = [line.rstrip("\n") for line in f]
    
    guesses = []
    for line in lines:
        attempt = breakSingleCharXor(line, threshhold)
        for e in attempt:
            guesses.append(e)
    return sorted(guesses, key=operator.itemgetter(2))
    
def repeatingKeyXor(msg, key):
    """
    XOR's ``msg`` with ``key``. Inputs are both strings.
    Returns a hex encoded string of the result.
    """
    # Create a repeating key string that's the same length as the message. Make
    # it one extra key longer to make sure it's at least as long as the message.
    # Truncate it afterwards to be exactly the length of the message.
    repeatingKey = (len(msg) // len(key)) * key + key
    repeatingKey = repeatingKey[:len(msg)]

    return xor(bin2hex(msg.encode()), bin2hex(repeatingKey.encode()))

