# Set 1

### Challenge 1: Convert hex to base64

The method to solve challenge 1 is pretty straight forward. I take the hex
encoded string and convert it into raw bytes using built-in python methods. I
then take the raw bytes and encode them in base64 again using built-in python
methods.

### Challenge 2: Fixed XOR

The purpose of this challenge is to take two hex encoded strings and to xor 
them together. To do this, I convert each string to raw bytes, then I use an
inline python loop to xor each pair of bytes together. The two strings must be
the same length. Once it's done, I hex encode the string and return it.

### Challenge 3: Single-byte XOR cipher

The purpose of this challenge is to find out which single character a string has
been xor'd with. For this, I will use letter frequency and score using the
chi-squared test. The chi-squared test looks at 2 different probability
distributions and tests how similar they are. More information is given on 
Wikipedia, as well as : http://www.practicalcryptography.com/cryptanalysis/text-characterisation/chi-squared-statistic/

### Challenge 4: Detect single-character XOR

The purpose of this challenge is to take an input file with multiple lines of 
hex encoded strings where only one of the lines has been xor'd against a single
character. The method to solving this is just an iterative method of repeating
the previous challenge. From the results of the previous challenge, we found 
that the score that gave an English result was around 33 or less. To detect
single-character XOR, I will only look at scores 50 and less to determine if I
think they have been XOR'd with a single character. 

Based on results of this challenge, it would be really good to consider using
di-grams, or a dictionary attack or something. The actual result showed up as 
\#14 in the list of results. This is likely due to the short string length.

### Challenge 5: Implement repeating-key XOR

The purpose of this challenge is to implement a repeating-key XOR function. This
is also known as the Vigenere cipher. Most of the existing code from previous
challenges can be reused for this challenge.

A message *p* is partitioned into *k* partitions such that:
*p = p_1 | p_2 | ... | p_k*. Each partition is of the same length as the 
encryption key. If the key is too long, then the key will be truncated to the
length of the last remaining partition. Each partition, *p_i* is XOR'd with
the key using the XOR function written in a previous challenge. Each XOR'd
partition, *c_i*, is concatenated together to give a ciphertext of 
*c = c_1 | c_2 | ... | c_k*. 
