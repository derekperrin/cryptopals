import common

# Letter frequencies from:
# https://www.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
inputFile = "data4.txt"


if __name__ == "__main__":
    threshhold = 50 # our threshhold for whether a guess is valid
    guesses = common.detectSingleCharXor(inputFile, threshhold)
    for i in range(len(guesses)):
        print("Guess #" + str(i + 1))
        print(str(guesses[i][0]))
        print("Key: " + str(guesses[i][1]))
        print("Score: " + str(guesses[i][2]))
        print("")
